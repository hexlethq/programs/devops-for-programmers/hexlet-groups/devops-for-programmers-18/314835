resource "digitalocean_droplet" "droplet" {
  count = 2

  name   = "ansible-for-servers-homerwork-${count.index}"
  image  = var.droplet.image
  region = var.droplet.region
  size   = var.droplet.size
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]

  connection {
    host = self.ipv4_address
    private_key = file(var.pvt_key)
    user = var.droplet.connection.user
    type = var.droplet.connection.type
    timeout = var.droplet.connection.timeout
  }
}

resource "digitalocean_loadbalancer" "load-balancer" {
  name = "ansible-for-servers-homework"
  region = var.droplet.region

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.droplet.*.id
}

resource "digitalocean_domain" "default" {
  name       = "ansible-for-servers.toomean.com"
  ip_address = digitalocean_loadbalancer.load-balancer.ip
}

output "droplets_ips" {
  value = digitalocean_droplet.droplet.*
}
