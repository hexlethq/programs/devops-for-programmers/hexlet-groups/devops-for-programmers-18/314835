variable "droplet" {
  type = object({
    image = string
    region = string
    size = string

    connection = object({
      user = string
      type = string
      timeout = string
    })
  })

  default = {
    image = "docker-20-04",
    region = "ams3",
    size = "s-1vcpu-1gb",

    connection = {
      user = "root",
      type = "ssh",
      timeout = "2m",
    }
  }
}
