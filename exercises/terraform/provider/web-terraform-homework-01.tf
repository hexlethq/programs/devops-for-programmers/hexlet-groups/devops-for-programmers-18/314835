resource "digitalocean_droplet" "web-terraform-homework-01" {
  image = "docker-20-04"
  name = "web-terraform-homework-01"
  region = "ams3"
  size = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
}
