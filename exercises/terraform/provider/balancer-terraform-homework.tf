resource "digitalocean_loadbalancer" "load-balancer" {
  name = "loadbalancer-terraform-homework"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = [
    digitalocean_droplet.web-terraform-homework-01.id,
    digitalocean_droplet.web-terraform-homework-02.id
  ]
}

resource "digitalocean_domain" "default" {
  name       = "terraform.toomean.com"
  ip_address = digitalocean_loadbalancer.load-balancer.ip
}

output "output_name" {
  value = [
    digitalocean_droplet.web-terraform-homework-01.ipv4_address,
    digitalocean_droplet.web-terraform-homework-02.ipv4_address
  ]
}