variable "do_token" {}
variable "pvt_key" {}

variable "ssh_key" {
  type = string
}

variable "subdomain" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "region" {
  type = string
  default = "ams3"
}

variable "name" {
  type = string
  default = "security"
}

variable "web_droplet_count" {
  type = number
  default = 1
}

variable "web_droplet_image" {
  type = string
  default = "ubuntu-20-04-x64"
}

variable "web_droplet_size" {
  type = string
  default = "s-1vcpu-1gb"
}

variable "bastion_droplet_size" {
  type = string
  default = "s-1vcpu-1gb"
}
