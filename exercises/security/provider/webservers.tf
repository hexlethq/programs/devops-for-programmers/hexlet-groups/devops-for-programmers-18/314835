resource "digitalocean_droplet" "web" {
  count = var.web_droplet_count

  image = var.web_droplet_image

  name = "security-web-${ var.name }-${ var.region }-${ count.index + 1 }"

  region = var.region

  size = var.web_droplet_size

  ssh_keys = [data.digitalocean_ssh_key.main.id]

  vpc_uuid = digitalocean_vpc.web.id

  tags = ["${ var.name }-web"]

  user_data = <<EOF
  #cloud-config
  packages:
    - nginx
  runcmd:
    - wget -P /var/www/html https://raw.githubusercontent.com/do-community/terraform-sample-digitalocean-architectures/master/01-minimal-web-db-stack/assets/index.html
    - sed -i "s/CHANGE_ME/web-${var.region}-${count.index +1}/" /var/www/html/index.html
  EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_certificate" "web" {
  name = "${ var.name }-certificate"

  type = "lets_encrypt"

  domains = ["${ var.subdomain }.${ data.digitalocean_domain.web.name }"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_loadbalancer" "web" {
  name = "web-${ var.region }-loadbalancer"
  region = var.region
  droplet_ids = digitalocean_droplet.web.*.id

  vpc_uuid = digitalocean_vpc.web.id

  redirect_http_to_https = true

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 80
    target_protocol = "http"

    certificate_id = digitalocean_certificate.web.id
  }

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"

    certificate_id = digitalocean_certificate.web.id
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_firewall" "web" {
  name = "${ var.name }-only-vpc-traffic"

  # The droplets to apply this firewall to                                   #
  droplet_ids = digitalocean_droplet.web.*.id

  #--------------------------------------------------------------------------#
  # Internal VPC Rules. We have to let ourselves talk to each other          #
  #--------------------------------------------------------------------------#
  inbound_rule {
    protocol = "tcp"
    port_range = "1-65535"
    source_addresses = [digitalocean_vpc.web.ip_range]
  }

  inbound_rule {
    protocol = "udp"
    port_range = "1-65535"
    source_addresses = [digitalocean_vpc.web.ip_range]
  }

  inbound_rule {
    protocol = "icmp"
    source_addresses = [digitalocean_vpc.web.ip_range]
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "1-65535"
    destination_addresses = [digitalocean_vpc.web.ip_range]
  }

  outbound_rule {
    protocol = "udp"
    port_range = "1-65535"
    destination_addresses = [digitalocean_vpc.web.ip_range]
  }

  outbound_rule {
    protocol = "icmp"
    destination_addresses = [digitalocean_vpc.web.ip_range]
  }

  #--------------------------------------------------------------------------#
  # Selective Outbound Traffic Rules                                         #
  #--------------------------------------------------------------------------#

  # DNS
  outbound_rule {
      protocol = "udp"
      port_range = "53"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # HTTP
  outbound_rule {
      protocol = "tcp"
      port_range = "80"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # HTTPS
  outbound_rule {
      protocol = "tcp"
      port_range = "443"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # ICMP (Ping)
  outbound_rule {
      protocol              = "icmp"
      destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_record" "web" {
  domain = data.digitalocean_domain.web.name
  type = "A"
  name = var.subdomain
  value = digitalocean_loadbalancer.web.ip
  ttl = 300
}