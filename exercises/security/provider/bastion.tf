resource "digitalocean_droplet" "bastion" {
  image = var.web_droplet_image

  name = "security-bastion-${ var.name }-${ var.region }"

  region = var.region

  size = var.bastion_droplet_size

  ssh_keys = [data.digitalocean_ssh_key.main.id]

  vpc_uuid = digitalocean_vpc.web.id

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_record" "bastion" {
  domain = data.digitalocean_domain.web.name

  type = "A"

  name = "bastion-${ var.name }-${ var.region }"

  value = digitalocean_droplet.bastion.ipv4_address

  ttl = 300
}

################################################################################
# Create firewall rules for allowing only ssh traffic to and from the bastion  #
################################################################################
resource "digitalocean_firewall" "bastion" {
    
  # Human friendly name of the firewall
  name = "${var.name}-only-ssh-bastion"

  # Droplets to apply the firewall to
  droplet_ids = [digitalocean_droplet.bastion.id]

  #--------------------------------------------------------------------------#
  # Rules to allow only ssh both inbound from the public internet and only   #
  # allow outbout ssh traffic into the VPC network. Also allow ping just for #
  # ease of use inside the VPC as well.                                      #
  #--------------------------------------------------------------------------#
  inbound_rule {
    protocol = "tcp"
    port_range = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "22"
    destination_addresses = [digitalocean_vpc.web.ip_range]
  }

  outbound_rule {
    protocol = "icmp"
    destination_addresses = [digitalocean_vpc.web.ip_range]
  }
}
